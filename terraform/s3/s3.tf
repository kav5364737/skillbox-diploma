provider "aws" {
    region = var.AWS_REGION
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "s3bucket-terraform-state"
  force_destroy = true 
}

resource "aws_s3_bucket_versioning" "versioning_terraform_state" {
  bucket = aws_s3_bucket.terraform_state.id
  versioning_configuration {
    status = "Enabled"
  }
}

# В DynamoDB создадим таблицу "terraform_locks" с первичным ключом "LockID" для использования блокировки
resource "aws_dynamodb_table" "terraform_locks" {
  name = "s3bucket-terraform-state-locks"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "LockID"
    
  attribute {
    name = "LockID"
    type = "S"
  }
}