import boto3

bucket_name = 's3bucket-terraform-state'

s3 = boto3.resource('s3')
versioning = s3.BucketVersioning(bucket_name)

# enable versioning
versioning.enable()