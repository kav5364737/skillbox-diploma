
#resource "aws_eip" "my_static_ip2" {
#  instance = aws_instance.my_webserver.id
#  tags = {
#    Name  = "Staging for Ansible"
#  }
#}

# Запускаем инстанс
resource "aws_instance" "my_webserver" {
  # с выбранным образом 
  ami                    = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t3.nano"
  vpc_security_group_ids = [aws_security_group.my_webserver.id]
  subnet_id  = aws_default_subnet.availability_zone_1.id
	user_data = <<-EOF
		#! /bin/bash
    sudo apt update -y
    sudo apt install nginx -y
    systemctl start nginx
    echo '127.0.0.1 ${var.server_staging}' | sudo tee -a /etc/hosts
    sudo hostnamectl set-hostname ${var.server_staging}
	EOF
  key_name = "test"
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Staging Server IP"
    Env = "Production"
    Tier = "Frontend"
    CM = "Ansible"
  }

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_security_group" "my_webserver" {
  name        = "Staging Security Group for Ansible"
  description = "Security group for accessing traffic to Staging server for Ansible"


  dynamic "ingress" {
    for_each = var.porttoopen_prod
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "ingress" {
    for_each = var.porttoopen_inside_prod
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["172.31.0.0/16"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Staging Server SecurityGroup for Ansible"
  }
}
