variable "porttoopen" {
  description = "Port to open insideout"
  type        = list
  default     = ["22", "80"]
}

variable "porttoopen_prod" {
  description = "Port to open insideout"
  type        = list
  default     = ["22", "80", "8080"]
}

variable "porttoopen_prometheus" {
  description = "Port to open insideout"
  type        = list
  default     = ["22", "80", "3000", "9090"]
}

variable "porttoopen_inside_prometheus" {
  description = "Port to open inside"
  type        = list
  default     = ["8500", "9090", "9100", "9093", "8080", "8301", "8300", "3100", "9080"]
}

variable "porttoopen_inside_prod" {
  description = "Port to open inside"
  type        = list
  default     = ["9100", "9093", "8080", "9080"]
}

variable "porttoopen_inside" {
  description = "Port to open inside"
  type        = list
  default     = ["9100", "9080"]
}

variable "servername" {
  description = "Name of the servers"
  type        = string
  default     = "WebServer in Auto Scalling Group"
}

variable "elb_listen_port" {
  description = "Port to listen"
  type        = string
  default     = "8080"
}

variable "elb_listen_protocol" {
  description = "Protocol to listen"
  type        = string
  default     = "http"
}

variable "server_glr" {
  description = "Name glr"
  type        = string
  default     = "glr.techbit.space"
}

variable "server_staging" {
  description = "Name staging"
  type        = string
  default     = "staging.techbit.space"
}

variable "server_prod" {
  description = "Name prod"
  type        = string
  default     = "prod"
}

variable "server_prometheus" {
  description = "Name prometheus"
  type        = string
  default     = "prometheus.techbit.space"
}

variable "domain" {
  description = "Protocol to listen"
  type        = string
  default     = "techbit.space"
}

variable "AWS_REGION" { default = "eu-north-1" }
