# Указываем, что мы хотим разворачивать окружение в AWS
provider "aws" {
  region = var.AWS_REGION
}

variable "DATEUTC_FIRSTDEPLOY" { default = "" }

variable "TRIGGER_TOKEN_GL" { default = "" } 


terraform {
  backend "s3" {
    bucket = "s3bucket-terraform-state"
    key    = "app/terraform.tfstate"
    dynamodb_table = "s3bucket-terraform-state-locks"
    encrypt        = true
  }
}

# Узнаём, какие есть Дата центры в выбранном регионе
data "aws_availability_zones" "available" {}

# Ищем образ с последней версией Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Созаём правило, которое будет разрешать трафик к нашим серверам
resource "aws_security_group" "web" {
  name = "Dynamic Security Group"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = var.porttoopen_prod
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "ingress" {
    for_each = var.porttoopen_inside_prod
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["172.31.0.0/16"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name  = "Web access for Application"
  }
}


# Создаём Launch Configuration - это сущность, которая определяет конфигурацию запускаемых серверов. Размер, , 

resource "aws_launch_configuration" "web" {
  name_prefix     = "Web-server-"
  # какой будет использоваться образ
  image_id        = data.aws_ami.ubuntu.id
  # Размер машины (CPU и память)
  instance_type   = "t3.nano"
  # какие права доступа
  security_groups = [aws_security_group.web.id]
  # какие следует запустить скрипты при создании сервера
	user_data = <<-EOF
		#! /bin/bash
    #set -x
    # устанавливаем nginx для health check
    sudo apt update -y
    sudo apt install nginx -y
    systemctl start nginx
    sudo apt install curl -y
    #берем текущую дату
    dateutc=$(date +%s)
    #сравниваем текущую дату с первым запуском pipeline и запускаем pipeline при создании нового сервера ASG
    if [ $dateutc -gt ${var.DATEUTC_FIRSTDEPLOY} ]
    then 
    sudo curl --request POST ${var.TRIGGER_TOKEN_GL}
    fi
    echo '127.0.0.1 $dateutc.${var.server_prod}.${var.domain}' | sudo tee -a /etc/hosts
    sudo hostnamectl set-hostname $dateutc.${var.server_prod}.${var.domain}
	EOF
  #iam_instance_profile = "AmazonEC2RoleForSSM"
  # какой SSH ключ будет использоваться 
  key_name = "test"
  # Если мы решим обновить инстанс, то, прежде, чем удалится старый инстанс, который больше не нужен, должен запуститься новый
  lifecycle {
    create_before_destroy = true
  }
}

# AWS Autoscaling Group для указания, сколько нам понадобится инстансов 
resource "aws_autoscaling_group" "web" {
  name                 = "ASG-${aws_launch_configuration.web.name}"
  launch_configuration = aws_launch_configuration.web.name
  min_size             = 2
  max_size             = 4
  min_elb_capacity     = 2
  desired_capacity     = 2
  health_check_type    = "ELB"
  # и в каких подсетях, каких Дата центрах их следует разместить
  vpc_zone_identifier  = [aws_default_subnet.availability_zone_1.id, aws_default_subnet.availability_zone_2.id]
  # Ссылка на балансировщик нагрузки, который следует использовать 
  load_balancers       = [aws_elb.web.name]
  force_delete         = true
  wait_for_capacity_timeout = "20m"
  dynamic "tag" {
    for_each = {
      Name   = var.servername
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }  
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "asg_policy_up" {
  name = "asg_policy_up"
  scaling_adjustment = 1
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  autoscaling_group_name = "ASG-${aws_launch_configuration.web.name}"
  depends_on = [aws_autoscaling_group.web]
}

resource "aws_cloudwatch_metric_alarm" "asg_cpu_alarm_up" {
  alarm_name = "asg_cpu_alarm_up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "300"
  statistic = "Average"
  threshold = "70"
  dimensions = {
    AutoScalingGroupName = "ASG-${aws_launch_configuration.web.name}"
  }
  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions = [aws_autoscaling_policy.asg_policy_up.arn]
  depends_on = [aws_autoscaling_group.web]
}

resource "aws_autoscaling_policy" "asg_policy_down" {
  name = "asg_policy_down"
  scaling_adjustment = -1
  adjustment_type = "ChangeInCapacity"
  cooldown = 1800
  autoscaling_group_name = "ASG-${aws_launch_configuration.web.name}"
  depends_on = [aws_autoscaling_group.web]
}

resource "aws_cloudwatch_metric_alarm" "asg_cpu_alarm_down" {
  alarm_name = "asg_cpu_alarm_down"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "300"
  statistic = "Average"
  threshold = "30"
  dimensions = {
    AutoScalingGroupName = "ASG-${aws_launch_configuration.web.name}"
  }
  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions = [aws_autoscaling_policy.asg_policy_down.arn]
  depends_on = [aws_autoscaling_group.web]
}

# Elastic Load Balancer проксирует трафик на наши сервера 
resource "aws_elb" "web" {
  name               = "WebServer-Highly-Available-ELB"
  # перенаправляет трафик на несколько Дата центров
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.web.id]
  # слушает на порту 8080
  listener {
    lb_port           = var.elb_listen_port
    lb_protocol       = var.elb_listen_protocol
    instance_port     = var.elb_listen_port
    instance_protocol = var.elb_listen_protocol
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 10
  }
  tags = {
    Name = "WebServer-Highly-Available-ELB"
  }
}

# Созаём подсети в разных Дата центрах
resource "aws_default_subnet" "availability_zone_1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "availability_zone_2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

# Выведем в консоль DNS имя нашего сервера 
output "web_loadbalancer_url" {
  value = aws_elb.web.dns_name
}
