
#resource "aws_eip" "my_static_ip1" {
#  instance = aws_instance.glr.id
#  tags = {
#    Name = "Gitlab-runner IP for Ansible"
#  }
#}

# Запускаем инстанс
resource "aws_instance" "glr" {
  # с выбранным образом 
  ami = data.aws_ami.ubuntu.id
  # и размером (количество ЦПУ и памяти зависит от этой директивы) 
  instance_type          = "t3.micro"
  vpc_security_group_ids = [aws_security_group.glr.id]
  subnet_id  = aws_default_subnet.availability_zone_1.id
	user_data = <<-EOF
		#! /bin/bash
    sudo apt update -y
    sudo apt install nginx -y
    systemctl start nginx
    echo '127.0.0.1 ${var.server_glr}' | sudo tee -a /etc/hosts
    sudo hostnamectl set-hostname ${var.server_glr}
	EOF
  key_name = "test"
  tags = {
    AMI   =  "${data.aws_ami.ubuntu.id}"
    Name  = "Gitlab-runner Server IP"
    Env   = "Production"
    Tier  = "Frontend"
    CM    = "Ansible"
  }
  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_security_group" "glr" {
  name        = "Gitlab-runner Security Group for Ansible"
  description = "Security group for accessing traffic to Gitlab-runner server for Ansible"


  dynamic "ingress" {
    for_each = var.porttoopen
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "ingress" {
    for_each = var.porttoopen_inside
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["172.31.0.0/16"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Gitlab-Runner Server SecurityGroup for Ansible"
  }
}
