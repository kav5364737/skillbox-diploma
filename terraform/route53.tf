resource "aws_s3_bucket" "redirect" {
  bucket = "${var.domain}"
  acl    = "private"

  website {
    redirect_all_requests_to = "http://${aws_elb.web.dns_name}:8080"
  }
}

resource "aws_s3_bucket" "www_redirect" {
  bucket = "www.${var.domain}"
  acl    = "private"

  website {
    redirect_all_requests_to = "http://${aws_elb.web.dns_name}:8080"
  }
}

resource "aws_s3_bucket" "monitoring" {
  bucket = "monitoring.${var.domain}"
  acl    = "private"

  website {
    redirect_all_requests_to = "${aws_eip.my_static_ip3.public_ip}:9090"
  }
}

resource "aws_s3_bucket" "grafana" {
  bucket = "grafana.${var.domain}"
  acl    = "private"

  website {
    redirect_all_requests_to = "${aws_eip.my_static_ip3.public_ip}:3000"
  }
}

resource "aws_route53_zone" "domain" {
  name = "${var.domain}"
}

resource "aws_route53_record" "domain" {
  name    = "${var.domain}"
  zone_id = "${aws_route53_zone.domain.zone_id}"
  type    = "A"

  alias {
    name                   = "${aws_s3_bucket.redirect.website_domain}"
    zone_id                = "${aws_s3_bucket.redirect.hosted_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "www_domain" {
  name    = "www.${var.domain}"
  zone_id = "${aws_route53_zone.domain.zone_id}"
  type    = "A"

  alias {
    name                   = "${aws_s3_bucket.www_redirect.website_domain}"
    zone_id                = "${aws_s3_bucket.www_redirect.hosted_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "monitoring" {
  name    = "monitoring.${var.domain}"
  zone_id = "${aws_route53_zone.domain.zone_id}"
  type    = "A"

  alias {
    name                   = "${aws_s3_bucket.monitoring.website_domain}"
    zone_id                = "${aws_s3_bucket.monitoring.hosted_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "grafana" {
  name    = "grafana.${var.domain}"
  zone_id = "${aws_route53_zone.domain.zone_id}"
  type    = "A"

  alias {
    name                   = "${aws_s3_bucket.grafana.website_domain}"
    zone_id                = "${aws_s3_bucket.grafana.hosted_zone_id}"
    evaluate_target_health = true
  }
}