# Краткое описание проекта
Дипломная работа по курсу "Devop-инженер. Основы." skillbox.ru

# Требования к инфраструктуре
Вся инфраструктура должна быть развернута с помощью terraform и ansible на облачных серверах AWS. В проекте можно использовать готовые решения terraform и ansible.

# Какие инструменты/утилиты/программы нужно установить предварительно 

1. Для развертывая инфраструктуры обязательно требуется установить на локальном компьютере Terraform, Ansible, AWS CLI, Git, Docker, Gitlab-runner.\
[Инструкция по установке Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)\
[Инструкция по установке Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#pipx-install)\
[Инструкция по установке AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)\
[Инструкция по установке Git](https://www.git-scm.com/downloads)\
[Инструкция по установке Docker](https://docs.docker.com/engine/install/)\
[Инструкция по установке Gitlab-runner](https://docs.gitlab.com/runner/install/linux-repository.html)

2. Дополнительно можно установить Visual Studio Code для удобства работы с исходным кодом, VirtualBox для создания локальной виртуальной машины для тестирования работоспособности ansible-кода с помощью Vagrant.\
[Инструкция по установке Visual Studio Code](https://code.visualstudio.com/Download)\
[Инструкция по установке Virtualbox](https://www.virtualbox.org/wiki/Downloads)\
[Инструкция по установке Vagrant](https://developer.hashicorp.com/vagrant/install)

3. Также требуется настройка AWS.
* Зарегистрируйте аккаунт в AWS и для начала [активируйте бесплатный доступ](https://aws.amazon.com/ru/free/activate-your-free-tier-account/).
* [Cконфигурируйте его для управления аккаунтом и ресурсами](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html).
* Перейдите в [настройки](https://us-east-2.console.aws.amazon.com/ec2/home?region=us-east-2#Home:), на панели навигации выберите Key Pairs и [загрузите свой SSH-ключ](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html) — его имя будет использоваться в Terraform-скрипте(файл /terraform/variables.tf переменная sshkeyname), а также в Ansible-скрипте(файл /ansible/hosts параметр ansible_ssh_private_key_file для каждлшл из серверов).
* Также если требуется необходимо зарегестрировать новый домен в AWS или у другого провайдера указав DNS-серверы Hosted-zone.

4. Для развертывания инфраструктуры и сервисов необходимо [зарегестрировать gitlab-runner типа docker и типа shell](https://docs.gitlab.com/runner/register/).

5. Требуется ввести следуюшие переменные в gitlab для проекта https://gitlab.com/kav5364737/skillbox-diploma.git:\
AWS_SSH_KEY - ssh-ключ(файл) для учетной записи AWS\
AWS_ACCESS_KEY - ID ключа доступа Key Pairs\
AWS_SECRET_ACCESS_KEY - секретный ключ Key Pairs\
AWS_REGION - регион AWS\
DH_USER - пользователь dockerhub\
DH_PASS - пароль dockerhub\
GLR_TOKEN - Токен для Gitlab-runner типа docker\
GLR_TOKEN2 - Токен для Gitlab-runner типа shell\
TRIGGER_TOKEN - Триггер токен для запуска пайплайна из проекта приложения (https://gitlab.com/kav5364737/skillbox-diploma-service.git) вместе с адресом (пример: "https://gitlab.com/api/v4/projects/52252860/trigger/pipeline?token="trigger token"&ref=main")

6. Требуется ввести следуюшие переменные в gitlab для проекта https://gitlab.com/kav5364737/skillbox-diploma-service.git:\
AWS_SSH_KEY - ssh-ключ(файл) для учетной записи AWS\
AWS_ACCESS_KEY - ID ключа доступа Key Pairs\
AWS_SECRET_ACCESS_KEY - секретный ключ Key Pairs\
AWS_REGION - регион AWS\
DH_USER - пользователь dockerhub\
DH_PASS - пароль dockerhub\

# Как пользоваться и развернуть проект

Проект делится на два подпроекта:

1. Первоначальное развертывание инфраструктуры и сервисов происходит из пуша в проект https://gitlab.com/kav5364737/skillbox-diploma.git.

С помощью terraform развертывается следующая инфраструктура: 1-4 prod сервера, 1 staging сервер, 1 gitlab-runner, Autoscaling Group для prod сеоверов, Load Balancer для prod севреров, Dinamic Security Group, AWS s3 bucket, DynamoDB, Route53 hosted zone.

С помощью Ansible на серверы устанавливаются следующие приложения: 
* на prod серверы устанавливается docker, запускается последний образ приложения, устанавливается ПО для мониторинга 
* на staging серверы устанавливается docker и запускается последний образ приложения, устанавливается ПО для мониторинга 
* на gitlab-runner сервер устанавливаются docker, aws cli, ansible, gitlab-runner, регистрируются gitlab-runner shell, gitlab-runner docker, устанавливается ПО для мониторинга 
* на monitoring сервер устанвливаются prometheus, grafana, loki, ПО для мониторинга 

После прохождения pipeline сервис доступен по адресу указанному в переменной domain /terraform/variables.tf (по умолчанию [techbit.space:8080](techbit.space:8080)). Возможно потребуется до 24 часов для обновления DNS-записи.

![techbit.space:8080](webpage_ok.png)

2. Вторая часть это проект https://gitlab.com/kav5364737/skillbox-diploma-service.git.

С помощью него осущесталяется разработка приложения: тестирование, сборка новых версий, развертывание на staging, тестирование доступности, ручная заливка на prod, откат на версию назад. Также через этот pipeline осуществляется деплой всей необходимой инфраструктуры и приложения при добавлении сервера AutoScalingGroup.

## 1. Развертывание инфраструктуры.

Pipeline можно запустить 2 способами: через интерфейс Gitlab: Build->Pipeline-> Run Pipeline выбрать ветку main или
сделать commit из скопированного репозитория:
```
sudo git add .; sudo git commit -a -m "init"; sudo git push
```

Pipeline содержит include: Terraform/Base.gitlab-ci.yml и состоит из слудующих job, которые выполняются последовательно:\

![Gitlab pipeline infrastructire](gitlab_pipeline.png)

fmt_aws: job terraform fmt используется для перезаписи файлов конфигурации Terraform в канонический формат и стиль\
validate_aws: job проверки файла конфигурации terraform(validatet terraform)\
first_deploy_or_not: job используется для проверки осуществлен ли уже деплой web серверов и s3 bucket\
s3_create: job в случае отсутсвия создает s3 bucket\
build_aws: job собирает конфигурацию terraform(terraform plan если еще не развернута, terraform plan -refresh-only если конфигурауия уже ращвернута)\
cleanup_aws: job удаления всех серверов развернутых с помощью terrsform(terraform destroy)\
cleanup_s3: job удаляет s3 bucket\
deploy_aws: job деплоя всей конфигурации terraform(terrafom apply)\
deploy_glr_infra: job развертывания инфраструктуры(docker), а также сервисов(aws cli, ansible, gitlab-runner, consul agent, prometheus-node-exporter, регистрация gitlab-runner shell, регистрация gitlab-runner docker) с помощью ansible\
deploy_staging_infra: job развертывания инфраструктуры(docker) и сервисов consul agent, prometheus-node-exporter с помощью ansible\
deploy_prod_infra: job развертывания инфраструктуры(docker) и сервисов consul agent, prometheus-node-exporter  с помощью ansible\
deploy_prometheus_infra: job развертывания promethaus, grafana, consul server, prometheus-node-exporter, prometheus-alertmanager, loki, promtail с помощью ansible\
deploy_staging_service:job развертывания сервиса(web прилодения) с помощью ansible\
deploy_prod_service:job развертывания сервиса(web прилодения) с помощью ansible

.build_awscli: скрытая job для сборки docker образа AWS CLI запускается по расписанию\
.prepare_aws_cred: набор скриптов для логина в AWS\
.awscli_request: набор скриптов для запроса в AWS внешник ip-адресов инстансов и упаковки их в файл hosts для использования c ansible\
.awscli_privatip: набор скриптов для запроса в AWS внутренних ip-адресов инстансов и consul server для использования их в конфигурировании consul server, consul agent

Terraform скрипты развертывают:
* 2-4 prod сервера t3.nano с операционной системой Ubuntu в 2-х разных Дата-центрах
* 1 staging сервера t3.nano с операционной системой Ubuntu
* 1 gitlab-runner сервер t3.micro с операционной системой Ubuntu
* 1 Monitoring сервер t3.micro с операционной системой Ubuntu
* Autoscaling Group
* Load Balancer
* Dinamic Security Group
* AWS s3 bucket
* DynamoDB
* Route53 hosted zone
* S3 bucket redirection

Ansible скрипты развертывают:
* На prod серверы устанавливается docker и запускается последний образ приложения 
* На staging серверы устанавливается docker и запускается последний образ приложения
* На gitlab-runner сервер устанавливаются docker, aws cli, ansible, gitlab-runner, регистрируются gitlab-runner shell, gitlab-runner docker
* На monitoring server устанавливается prometheus, grafana, consul server, prometheus-node-exporter

Autoscaling Group содержит правило для развертывания нового сервера при загрузке CPU на 70% больше чем на 5 минут при этом при развертывании сервера происходит запуск pipeline trigger для заливки всего необходимого ПО на новый instance из проекта приложения (https://gitlab.com/kav5364737/skillbox-diploma-service.git). Удаление серверов происходит при условии загрузки CPU севреров ниже 30% в течении 5 минут.

## 2. Развертывание приложения.

Pipeline можно запустить 2 способами: через интерфейс Gitlab: Build->Pipeline-> Run Pipeline выбрать ветку main или
сделать commit из скопированного репозитория:
```
sudo git add .; sudo git commit -a -m "init"; sudo git push
```
Для работы pipeline требуется активировать gitlab-runner'ы для проекта. 

Pipeline из слудующих job которые выполняются последовательно:

![Gitlab pipeline service](gitlab_pipeline2.png)

staging-test: job предварительного тестирования перед сборкой\
build_app: job сборки image приложения\
deploy_staging: job развертывания нового image на staging сервер\
test_access: job тестирования доступности сервиса после деплоя на staging сервере\
deploy_prod: job резвертывания image на prod-серверы\
deploy_prod_trigger: job развертывания ansible инфраструктуры и сервиса, а также consul agent, prometheus-node-exporter, promtail на prod-серверы при срабатывании pipeline trigger token при добавлении нового сервера AutoScalingGroup\
revert_prod: job отката на предыдущий image на prod серверах

.prepare_aws_cred: набор скриптов для логина в AWS\
.awscli_request: набор скриптов для запроса в AWS ip-адресов инстансов для подключения ssh и запуска контейнера приложения\
.awscli_request2: набор скриптов для запроса в AWS ip-адресов инстансов и упаковки их в файл hosts для использования c ansible в задаче deploy_prod_trigger\
.awscli_privatip: набор скриптов для запроса в AWS внутренних ip-адресов инстансов и consul server для использования их в конфигурировании consul server, consul agent

# Состав серверов проекта

* 1-4 prod сервера t3.nano с операционной системой Ubuntu в 2-х разных Дата-центрах с установленным docker и запущенным контерйнером web-приложения
* 1 staging сервера t3.nano с операционной системой Ubuntu с установленным docker и запущенным контерйнером web-приложения
* 1 gitlab-runner сервер t3.micro с операционной системой Ubuntu с установленными docker, aws cli, ansible, gitlab-runner, регистрируются gitlab-runner shell, gitlab-runner docker
* 1 Monitoring сервер t3.micro с операционной системой Ubuntu с установленными promethaus, grafana, consul server, prometheus-node-exporter, prometheus-alertmanager, loki, promtail
* Autoscaling Group
* Load Balancer
* Dinamic Security Group
* AWS s3 bucket
* DynamoDB
* Route53 hosted zone
* S3 bucket redirection

# Схема взаимодействия компонентов/серверов
![Схема взаимодействия компонентов/серверов](Schema.png)

# Описание файлов и их назначение
## Проект для развертывания инфраструктуры и сервисов с нуля https://gitlab.com/kav5364737/skillbox-diploma.git

/README.md инструкция по проектам\
/Schema.png схема проекта\
/vagrantfile файл конфигурации Vagrant для развертывания тестовой виртуальной машины\
/terraform/s3/main.tf terraform-код для развертывания бэкэнда для хранения состояния конфигурации terraform в Amazon S3\
/terraform/s3/variables.tf файл с описанием переменных /terraform/s3/main.tf\
/terraform/s3/removes3.py скрипт boto3 для удаления s3 bucket\
/terraform/main.tf terraform-код для развертывания инфраструктуры web-серверов\
/terraform/glr.tf terraform-код для развертывания инфраструктуры gitlab-runner сервера\
/terraform/staging.tf terraform-код для развертывания инфраструктуры staging сервера\
/terraform/route53.tf terraform-код для создание DNS-записей и s3 bucket redirect на адресы monitoring,grafana,www\
/terraform/user_data.sh скрипт для установки nginx после развертывания инфраструктуры для прохождения Health Check\
/terraform/variables.tf файл с описанием переменных /terraform/main.tf\
/awscli/Dockerfile dockerfile для сборки образа с AWS CLI\
/ansible/ansible.cfg файл конфигурации ansible\
/ansible/hosts файл с описанием хостов ansible\
/ansible/main.yml основной playbook ansible\
/ansible/provision.sh cкрипт обработчик запроса в AWS для создания hosts файла ansible\
/ansible/roles/infra папка роли ansible для развертывания инфраструктуры(docker) для web-приложения\
/ansible/roles/service папка роли ansible для развертывания сервиса для web-приложения\
/ansible/roles/gitlab-runner папка роли ansible для развертывания приложений gitlab-runner сервера\
/ansible/roles/prometheus папка роли ansible для развертывания приложений monitoring сервера\
/ansible/roles/prometheus-client папка роли ansible для развертывания сервисов-клиентов(node-exporter,consul agent,promtail) prometheus\
/ansible/roles/prometheus-client-prod папка роли ansible для развертывания сервисов-клиентов(node-exporter,consul agent, сервиса web приложения,promtail) prometheus
/doc/adr/monitoring/README.md LADR о внедрении системы мониторинга

## Проект для разработки приложения https://gitlab.com/kav5364737/skillbox-diploma-service.git

/README.md инструкция по проекту\
/deploy_service.sh  скрипт развертывания сервиса(запуск docker image)\
/ansible/provision.sh cкрипт обработчик запроса в AWS для запроса ip-адресов для подключения ssh\
/ansible/provision2.sh cкрипт обработчик запроса в AWS для создания hosts файла ansible\
/run-tests.sh скрипт для запуска тестирования
/Dockerfile dockerfile для сборки приложения
/docker-compose.yaml dockerfile для сборки приложения
/cmd/server набор скриптов для сборки docker image приложения\
/scripts набор скриптов для тестирования приложения

# Мониторинг

[Запись решения о постановке на мониторинг](/doc/adr/monitoring/README.md)

## В качестве системы мониторинга была выбрана Prometheus + Grefana потому что:

* Простая настройка
* Маленькие требования к аппаратной части
* Быстрое развертывание
* Высокая производительность
* Простой и понятный интерфейс
* Поддерживает много пакетов, снимающих метрики с разных сервисов
* Удобный язык запросов
* Отличная визуальная часть

Для работы мониторинга ребуется ввести следуюшие переменные в gitlab для проекта https://gitlab.com/kav5364737/skillbox-diploma.git:\
GRAFANA_PASS - пароль для grafana\
PROM_PASS - пароль для prometheus\
PROM_EMAIL - электронный ящик gmail для отправки оповещений\
PROM_EMAIL_PASS - специпльно сгенеринованный для данного проекта пароль от электронного ящика gmail

## Состав проекта:

* Prometheus - основной компонент, получает метрики из разных сервисов и собирает их в одном месте. Prometheus собирает данные с экземпляров Node Exporter, а также метрики Web приложения.
* Grafana отображает данные из Prometheus в виде графиков и диаграмм, организованных в дашборды.
* Consul server - service discovery сервис для сбора данных с consul agent'ов инстансов в том числе развернутых через autoscling group.
* Prometeheus alertmanager - компонент для отправки оповещений триггеров, описанных в правилах prometheus.
* Node exporter — приложение, собирающее метрики операционной системы и предоставляющее к ним доступ по HTTP. Доступно на порту 9100.
* Метрики web-приложения доступны после запуска контейнера web-приложения на порту 8080.
* Blackbox-exporter - приложение отслеживающее состояние web страницы сервера web-приложения(ICMP, http 200)
* Consul agent - приложение отправляет данные по имеющимся на инстансе сервисам для мониторинга
* Loki - приложение для сбора логов с инстансов
* Promtail - приложение для отправки логов(syslog) на Loki
* RSyslog - отправляет syslog логи в promtail для корректного проставления label

## Схема мониторинга

![Схема мониторинга](monitoring.png)

## Для мониторинга серверов, prometheus и состояния инстансов спользуются следующие триггеры:

| N  | job name   | alert                                                                                      | duration | trigger | severity |
|----|------------|--------------------------------------------------------------------------------------------|----------|---------|----------|
| 1  | node       | {{ .instance }} of job {{ .job }} has been down for more than 5 minutes.                   | 5m       | state   | page     |
| 2  | node       | Node memory is filling up (< 10% left)                                                     | 2m       | <10%    | warning  |
| 3  | node       | Disk is almost full (< 10% left)                                                           | 2m       | <10%    | warning  |
| 4  | node       | Disk is almost running out of available inodes (< 10% left)                                | 2m       | <10%    | warning  |
| 5  | node       | CPU load is > 80%                                                                          | 10m      | >80%    | warning  |
| 6  | node       | Swap is filling up (>80%)                                                                  | 2m       | >80%    | warning  |
| 7  | node       | systemd service crashed                                                                    | 0m       | state   | warning  |
| 8  | node       | Host interface has encountered transmit errors in the last two minutes                     | 2m       | >1%     | warning  |
| 9  | node       | The network interface is getting overloaded.                                               | 1m       | >80%    | warning  |
| 10 | node       | Clock skew detected. Clock is out of sync. Ensure NTP is configured correctly on this host | 2m       | >0,05   | warning  |
| 15 | prometheus | A Prometheus job has disappeared                                                           | 0m       | state   | critical |
| 16 | prometheus | A Prometheus target has disappeared. An exporter might be crashed                          | 0m       | state   | warning  |
| 17 | prometheus | A Prometheus job does not have living target anymore                                       | 0m       | state   | warning  |
| 18 | prometheus | Prometheus configuration reload error                                                      | 0m       | state   | warning  |
| 19 | prometheus | A Prometheus AlertManager job has disappeared                                              | 0m       | state   | critical |
| 20 | prometheus | AlertManager configuration reload error                                                    | 0m       | state   | warning  |

После установки prometheus сервера по адресу monitoring.techbit.space доступна страница prometheus. Инстансы автоматически добавляются service discovery consul. Пароль можно взять из переменной gitlab PROM_PASS, логин admin.
![prometheus screenshot](prometheus.png)

После установки prometheus сервера по адресу grafana.techbit.space доступна страница grafana. В качестве Дашборда используется шаблонный 1860 "Node exporter full" с добавлением графиков и метрик Web-приложения, 7587 "blackmail", дашборд "syslog from rsyslog". Пароль можно взять из переменной gitlab GRAFANA_PASS, логин admin. После первого запуска требуется добавить источники данных: для prometheus http://localhost:9090(указать логин пароль для promethaus), для loki http://localhost:3100.
![grafana screenshot](grafana.png)

В качестве системы управления логами используеься Loki. В качестве Дашборда используется шаблонный 13639 "log/app". Обоснование выбора системы управления логами: ![google docs document](https://docs.google.com/document/d/1Y5F7c79HGRajBgsw3wstJkkrV0NZbe3fU8z_8SXRb8E/edit?usp=sharing)
![logs](logs.png)

Оповещения при сработки триггеров приходят на электронную почту gmail.
![gmail screenshot](alertmanager.png)

# Тестирование ansible-кода

Для тестирования ansible-кода проще использовать Vagrant на локальной виртуальной машине в VirtualBox.
1. Для начала потребуется внести изменения в файлы конфигурации ansible.
* Закомментировать строку inventory в файле /ansible/ansible.cfg
* Заменить в файле /ansible/main.yml в строке hosts значение app_servers на all
2. В папке проекта запускаем 
```
sudo vagrant up
```
3. Плейбук main.yml для установки двух ролей infra(для развертывания необходимых приложений) и service(для запуска самого сервиса) запустится автоматически.
4. Готово. Сервис доступен по адресу 192.168.56.10:8080.
5. Для удаления виртуальной машины вводим команду
```
sudo vagrant destroy
```
# План дальнейшего развития платформы

План задач на следующие 3-6 месяцев для двоих инженеров эксплуатации для улучшения и доработки системы:

[План развития](https://docs.google.com/document/d/14n3Mrq-5r0T3H2eKSzcCf4soUDEysm6Z14gB1eQyLw4/edit?usp=sharing)