datacenter = "dc1"
data_dir = "/opt/consul"
advertise_addr = "PRIVAT_IP"
encrypt = "SvfO8NKtQucedg53drxkE+Ipku+sAR8FwsluIIKc4ZI="
retry_join = ["PRIVAT_SERVER_IP"]
server = false
leave_on_terminate = true
rejoin_after_leave = true