include:
 - template: Terraform/Base.gitlab-ci.yml   

variables:
  IGNORE_TF_DEPRECATION_WARNING: true
  ANS_DIR: ansible
  tag: latest
  TF_ROOT: terraform

stages:
 - fmt
 - validate
 - build
 - deploy_aws
 - cleanup
 - deploy_infra
 - deploy_service

.build_awscli:
  stage: build
  image: docker
  services:
    - docker:dind
#  rules:
#    - if: $CI_PIPELINE_SOURCE == "schedule"
  script:
    - apk add git
    - git clone https://gitlab.com/kav5364737/skillbox-diploma.git
    - cd skillbox-diploma/awscli/
    - docker login -u "$DH_USER" -p "$DH_PASS" docker.io
    - docker build --pull -t tonic153/awscli:latest .
    - docker push index.docker.io/tonic153/awscli

.prepare_aws_cred:
  script:
    - rm ~/.aws/credentials || echo "already exist"
    - aws configure set aws_access_key_id $AWS_ACCESS_KEY
    - aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
    - aws configure set region $AWS_REGION

.awscli_request:
  script:
    - !reference [.prepare_aws_cred, script]
    - aws --region eu-north-1 ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values='$SERVER_TAG'" --query 'Reservations[*].Instances[*].[PublicIpAddress]' --output text > $CI_PROJECT_DIR/${CIVMNAME}_vm_ip.txt   
    - ./$ANS_DIR/provision.sh $CI_PROJECT_DIR $CIVMNAME $ANS_DIR
    - cat $CI_PROJECT_DIR/$ANS_DIR/hosts
    - mkdir ~/.ssh || echo "already exist"
    - cat $AWS_SSH_KEY > ~/.ssh/test.pem || echo "already exist"
    - chmod 400 ~/.ssh/test.pem

.awscli_privatip:
  script:
    - !reference [.prepare_aws_cred, script]
    - aws --region eu-north-1 ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values='$SERVER_TAG'" --query 'Reservations[*].Instances[*].[PrivateIpAddress]' | grep -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | tr -d ' ' | tr -d '"'  > $CI_PROJECT_DIR/vm_privatip.txt
    - aws --region eu-north-1 ec2 describe-instances --filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values=Prometheus Server IP" --query 'Reservations[*].Instances[*].[PrivateIpAddress]' | grep -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | tr -d ' ' | tr -d '"' > $CI_PROJECT_DIR/privat_server_ip.txt

fmt_aws:
  stage: fmt
  extends: .terraform:fmt
  tags: 
    - docker
    
validate_aws:
  extends: .terraform:validate
  needs:
    - s3_create
  tags: 
    - docker

first_deploy_or_not:
  stage: validate
  script:
    - !reference [.prepare_aws_cred, script]
    - FIRST_DEPLOY=$(aws --region eu-north-1 ec2 describe-instances  --filters  "Name=instance-state-name,Values=running"  "Name=tag:Name,Values=WebServer in Auto Scalling Group"  --query 'Reservations[*].Instances[*].[PublicIpAddress]' | wc -l)
    - S3_DEPLOY=$(aws s3 ls | wc -l)
    - echo "FIRST_DEPLOY=$FIRST_DEPLOY" > deploy.env
    - echo "S3_DEPLOY=$S3_DEPLOY" >> deploy.env
  artifacts:
    reports:
      dotenv: deploy.env
  tags: 
    - shell

s3_create:
  stage: validate
  variables:
    TF_ROOT: terraform/s3
  script:
    - cd $TF_ROOT
    - if [ "$S3_DEPLOY" -eq 0 ]; then gitlab-terraform validate; else echo "s3 bucket ready"; fi
    - if [ "$S3_DEPLOY" -eq 0 ]; then gitlab-terraform plan; else echo "s3 bucket ready"; fi
    - if [ "$S3_DEPLOY" -eq 0 ]; then gitlab-terraform apply; else echo "s3 bucket ready"; fi
  needs:
    - first_deploy_or_not
  tags:
    - docker

build_aws:
  stage: build
  variables:
    TF_VAR_TRIGGER_TOKEN_GL: ${TRIGGER_TOKEN}
    TF_VAR_DATEUTC_FIRSTDEPLOY: ${DATEUTC_FIRSTDEPLOY}
    TF_BACKEND_CONF: -backend-config="token=${TF_VAR_TRIGGER_TOKEN_GL}" -backend-config="token=${TF_VAR_DATEUTC_FIRSTDEPLOY}"
  before_script:
    - TF_VAR_DATEUTC_FIRSTDEPLOY=$(echo $(date +%s) + 600|bc)
  script: 
    - if [ "$FIRST_DEPLOY" -eq 1 ]; then gitlab-terraform plan -lock=false; else gitlab-terraform plan -refresh-only -lock=false; fi
    - if [ "$FIRST_DEPLOY" -eq 1 ]; then gitlab-terraform plan-json -lock=false; else gitlab-terraform plan-json -refresh-only -lock=false; fi
  when: always
  resource_group: ${TF_STATE_NAME}
  artifacts:
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json
  dependencies:
    - first_deploy_or_not
  tags: 
    - docker

cleanup_aws:
  extends: .terraform:destroy
  when: manual
  script:
    - gitlab-terraform destroy -lock=false
  dependencies:
    - build_aws
  tags: 
    - docker

cleanup_s3:
  stage: cleanup
  image: index.docker.io/tonic153/awscli
  variables:
    TF_ROOT: terraform/s3
  script:
    - !reference [.prepare_aws_cred, script]
    - apt-get -y install python3-pip 
    - pip3 install boto3
    - python3 $TF_ROOT/removes3.py
    - aws --debug dynamodb delete-table --table-name s3bucket-terraform-state-locks
  when: manual
  dependencies:
    - cleanup_aws
  tags:
    - docker

deploy_aws:
  stage: deploy_aws
  script:
    - gitlab-terraform apply -lock=false
  rules:
  tags:
    - docker

deploy_glr_infra:
  stage: deploy_infra
  variables: 
    CIVMNAME: glr
    SERVER_TAG: "Gitlab-runner Server IP"
  script:
    - !reference [.awscli_request, script]
    - !reference [.awscli_privatip, script]
    - cd $ANS_DIR
    - ansible-playbook main.yml --tags "infra" --diff
    - ansible-playbook main.yml --tags "prometheus-client" --diff
    - ansible-playbook main.yml --tags "gitlab-runner" -e "GLR_TOKEN=${GLR_TOKEN} GLR_TOKEN2=${GLR_TOKEN2}" --diff
  when: always
  tags: 
    - shell

deploy_staging_infra:
  stage: deploy_infra
  variables:
    CIVMNAME: staging
    SERVER_TAG: "Staging Server IP"
  script:
    - !reference [.awscli_request, script]
    - !reference [.awscli_privatip, script]
    - cd $ANS_DIR
    - ansible-playbook main.yml --tags "infra" --diff
    - ansible-playbook main.yml --tags "prometheus-client-prod" --diff
  when: always
  tags: 
    - shell

deploy_staging_service:
  stage: deploy_service
  variables:
    CIVMNAME: staging
    SERVER_TAG: "Staging Server IP"
  script:
    - !reference [.awscli_request, script]
    - cd $ANS_DIR
    - ansible-playbook main.yml --tags "service" --diff
  when: always
  tags: 
    - shell

deploy_prod_infra:
  stage: deploy_infra
  variables:
    CIVMNAME: app
    SERVER_TAG: "WebServer in Auto Scalling Group"
  script:
    - !reference [.awscli_request, script]
    - !reference [.awscli_privatip, script]
    - cd $ANS_DIR
    - ansible-playbook main.yml --tags "infra" --diff
    - ansible-playbook main.yml --tags "prometheus-client-prod" --diff
#  dependencies:
#    - build_aws
  when: always
  tags: 
    - shell

deploy_prod_service:
  stage: deploy_service
  variables:
    CIVMNAME: app
    SERVER_TAG: "WebServer in Auto Scalling Group"
  script:
    - !reference [.awscli_request, script]
    - cd $ANS_DIR
    - ansible-playbook main.yml --tags "service" -e "tag=${tag}" --diff
  dependencies:
    - deploy_prod_infra
  when: always
  tags: 
    - shell

deploy_prometheus_infra:
  stage: deploy_infra
  variables: 
    CIVMNAME: prom
    SERVER_TAG: "Prometheus Server IP"
  script:
    - !reference [.awscli_request, script]
    - !reference [.awscli_privatip, script]
    - cd $ANS_DIR
    - ansible-playbook main.yml --tags "prometheus" -e "GRAFANA_PASS=${GRAFANA_PASS} PROM_PASS=${PROM_PASS} PROM_EMAIL=${PROM_EMAIL} PROM_EMAIL_PASS=${PROM_EMAIL_PASS}" --diff
  when: always
  tags: 
    - shell
